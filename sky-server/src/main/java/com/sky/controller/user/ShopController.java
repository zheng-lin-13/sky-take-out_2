package com.sky.controller.user;

import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController("userShopController")
@RequestMapping("/user/shop")
@Slf4j
@Api(tags = "用户端商家状态相关接口")
public class ShopController {
    private static final String SHOPSTATUS= "SHOP_STATUS";
    @Autowired
    RedisTemplate redisTemplate;


    @GetMapping("/status")
    @ApiOperation("用户端获取商家状态")
    public Result<Integer> getStatus(){
        //从redis,获取营业状态
        Integer shop_status = (Integer) redisTemplate.opsForValue().get(SHOPSTATUS);
        //3.做出响应
        return Result.success(shop_status);
    }
}
